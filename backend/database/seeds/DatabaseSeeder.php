<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->name = 'Ivan Velkov';
        $user->email = 'vankata16@email.com';
        $user->password = bcrypt('q1w2e3');
        $user->save();
    }
}
