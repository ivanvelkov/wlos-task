import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';


import { GuestGuardService } from './guest-guard.service';

describe('GuestGuardService', () => {
  let service: GuestGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.inject(GuestGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
