import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // Variables
  authUrl = 'http://127.0.0.1:8000/oauth/token';
  apiUrl = 'http://127.0.0.1:8000/api';
  options: any;
  /**
   * Constructor
   * @param http The http client object
   */
  constructor(
    private http: HttpClient
  ) {
    this.options = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-Type': 'application/json'
      })
    };
  }
  /**
   * Get an access token
   * @param e The email address
   * @param p The password string
   */
  login(e: string, p: string) {
    return this.http.post(this.authUrl, {
      grant_type: 'password',
      client_id: '6',
      client_secret: '5zcTq27rlqgPF4J613ulyS0FtyOjIzaw79ZGivfJ',
      email: e,
      username: e,
      password: p,
      scope: ''
    }, this.options);
  }
  /**
   * Revoke the authenticated user token
   */
  logout() {
    // this.options.headers.Authorization = 'Bearer ' + localStorage.getItem('access_token');
    this.options.params = {
      'user_id': localStorage.getItem('user_id')
    }

    let headers = ({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': `Bearer ` + localStorage.getItem('access_token')
    });

    return this.http.get(this.apiUrl + '/token/revoke?user_id=1', {'headers': headers});
  }
}