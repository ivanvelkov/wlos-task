import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthService } from './auth.service';


describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });

    service = TestBed.inject(AuthService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should login', () => {
    const email = 'test@email.com';
    const password = 'password';
    const response = {
      access_token: 'abcdefghijklmnopqrstuvwxyz'
    };

    service.login(email, password).subscribe(res => {
      expect(res).toEqual(response);
    });

    const req = httpMock.expectOne(service.authUrl);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      grant_type: 'password',
      client_id: '6',
      client_secret: '5zcTq27rlqgPF4J613ulyS0FtyOjIzaw79ZGivfJ',
      email: email,
      username: email,
      password: password,
      scope: ''
    });
    req.flush(response);
  });

  it('should logout', () => {
    const response = {};
    localStorage.setItem('access_token', 'abcdefghijklmnopqrstuvwxyz');
    localStorage.setItem('user_id', '1');

    service.logout().subscribe(res => {
      expect(res).toEqual(response);
    });

    const req = httpMock.expectOne(service.apiUrl + '/token/revoke?user_id=1');
    expect(req.request.method).toBe('GET');
    expect(req.request.headers.get('Authorization')).toBe('Bearer abcdefghijklmnopqrstuvwxyz');
    req.flush(response);
  });
});
